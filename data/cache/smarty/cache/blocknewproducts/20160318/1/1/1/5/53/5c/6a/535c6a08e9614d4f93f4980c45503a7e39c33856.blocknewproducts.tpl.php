<?php /*%%SmartyHeaderCode:23218237356eb5206b0ac30-88292406%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '535c6a08e9614d4f93f4980c45503a7e39c33856' => 
    array (
      0 => '/Volumes/sites/wwwroot/newshop.dev/themes/default-bootstrap/modules/blocknewproducts/blocknewproducts.tpl',
      1 => 1452070228,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '23218237356eb5206b0ac30-88292406',
  'variables' => 
  array (
    'link' => 0,
    'new_products' => 0,
    'newproduct' => 0,
    'PS_CATALOG_MODE' => 0,
    'restricted_country_mode' => 0,
    'priceDisplay' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.19',
  'unifunc' => 'content_56eb5206be6912_67708567',
  'cache_lifetime' => 31536000,
),true); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_56eb5206be6912_67708567')) {function content_56eb5206be6912_67708567($_smarty_tpl) {?><!-- MODULE Block new products -->
<div id="new-products_block_right" class="block products_block">
	<h4 class="title_block">
    	<a href="http://newshop.dev/zh/new-products" title="本周新品">本周新品</a>
    </h4>
    <div class="block_content products-block">
                    <ul class="products">
                                    <li class="clearfix">
                        <a class="products-block-image" href="http://newshop.dev/zh/tshirts/1-faded-short-sleeve-tshirts.html" title=""><img class="replace-2x img-responsive" src="http://newshop.dev/1-small_default/faded-short-sleeve-tshirts.jpg" alt="Faded Short Sleeve T-shirts" /></a>
                        <div class="product-content">
                        	<h5>
                            	<a class="product-name" href="http://newshop.dev/zh/tshirts/1-faded-short-sleeve-tshirts.html" title="Faded Short Sleeve T-shirts">Faded Short Sleeve T-shirts</a>
                            </h5>
                        	<p class="product-description">Faded short sleeve t-shirt with high neckline. Soft and stretchy...</p>
                                                        	                                    <div class="price-box">
                                        <span class="price">
                                        	¥ 19.32                                        </span>
                                        
                                    </div>
                                                                                    </div>
                    </li>
                                    <li class="clearfix">
                        <a class="products-block-image" href="http://newshop.dev/zh/blouses/2-blouse.html" title=""><img class="replace-2x img-responsive" src="http://newshop.dev/7-small_default/blouse.jpg" alt="Blouse" /></a>
                        <div class="product-content">
                        	<h5>
                            	<a class="product-name" href="http://newshop.dev/zh/blouses/2-blouse.html" title="Blouse">Blouse</a>
                            </h5>
                        	<p class="product-description">Short sleeved blouse with feminine draped sleeve detail.</p>
                                                        	                                    <div class="price-box">
                                        <span class="price">
                                        	¥ 31.59                                        </span>
                                        
                                    </div>
                                                                                    </div>
                    </li>
                                    <li class="clearfix">
                        <a class="products-block-image" href="http://newshop.dev/zh/casual-dresses/3-printed-dress.html" title=""><img class="replace-2x img-responsive" src="http://newshop.dev/8-small_default/printed-dress.jpg" alt="Printed Dress" /></a>
                        <div class="product-content">
                        	<h5>
                            	<a class="product-name" href="http://newshop.dev/zh/casual-dresses/3-printed-dress.html" title="Printed Dress">Printed Dress</a>
                            </h5>
                        	<p class="product-description">100% cotton double printed dress. Black and white striped top and orange...</p>
                                                        	                                    <div class="price-box">
                                        <span class="price">
                                        	¥ 30.42                                        </span>
                                        
                                    </div>
                                                                                    </div>
                    </li>
                                    <li class="clearfix">
                        <a class="products-block-image" href="http://newshop.dev/zh/evening-dresses/4-printed-dress.html" title=""><img class="replace-2x img-responsive" src="http://newshop.dev/10-small_default/printed-dress.jpg" alt="Printed Dress" /></a>
                        <div class="product-content">
                        	<h5>
                            	<a class="product-name" href="http://newshop.dev/zh/evening-dresses/4-printed-dress.html" title="Printed Dress">Printed Dress</a>
                            </h5>
                        	<p class="product-description">Printed evening dress with straight sleeves with black thin waist belt...</p>
                                                        	                                    <div class="price-box">
                                        <span class="price">
                                        	¥ 59.66                                        </span>
                                        
                                    </div>
                                                                                    </div>
                    </li>
                                    <li class="clearfix">
                        <a class="products-block-image" href="http://newshop.dev/zh/summer-dresses/5-printed-summer-dress.html" title=""><img class="replace-2x img-responsive" src="http://newshop.dev/12-small_default/printed-summer-dress.jpg" alt="Printed Summer Dress" /></a>
                        <div class="product-content">
                        	<h5>
                            	<a class="product-name" href="http://newshop.dev/zh/summer-dresses/5-printed-summer-dress.html" title="Printed Summer Dress">Printed Summer Dress</a>
                            </h5>
                        	<p class="product-description">Long printed dress with thin adjustable straps. V-neckline and wiring...</p>
                                                        	                                    <div class="price-box">
                                        <span class="price">
                                        	¥ 33.91                                        </span>
                                        
                                    </div>
                                                                                    </div>
                    </li>
                                    <li class="clearfix">
                        <a class="products-block-image" href="http://newshop.dev/zh/summer-dresses/6-printed-summer-dress.html" title=""><img class="replace-2x img-responsive" src="http://newshop.dev/16-small_default/printed-summer-dress.jpg" alt="Printed Summer Dress" /></a>
                        <div class="product-content">
                        	<h5>
                            	<a class="product-name" href="http://newshop.dev/zh/summer-dresses/6-printed-summer-dress.html" title="Printed Summer Dress">Printed Summer Dress</a>
                            </h5>
                        	<p class="product-description">Sleeveless knee-length chiffon dress. V-neckline with elastic under the...</p>
                                                        	                                    <div class="price-box">
                                        <span class="price">
                                        	¥ 35.69                                        </span>
                                        
                                    </div>
                                                                                    </div>
                    </li>
                                    <li class="clearfix">
                        <a class="products-block-image" href="http://newshop.dev/zh/summer-dresses/7-printed-chiffon-dress.html" title=""><img class="replace-2x img-responsive" src="http://newshop.dev/20-small_default/printed-chiffon-dress.jpg" alt="Printed Chiffon Dress" /></a>
                        <div class="product-content">
                        	<h5>
                            	<a class="product-name" href="http://newshop.dev/zh/summer-dresses/7-printed-chiffon-dress.html" title="Printed Chiffon Dress">Printed Chiffon Dress</a>
                            </h5>
                        	<p class="product-description">Printed chiffon knee length dress with tank straps. Deep v-neckline.</p>
                                                        	                                    <div class="price-box">
                                        <span class="price">
                                        	¥ 19.19                                        </span>
                                        
                                    </div>
                                                                                    </div>
                    </li>
                            </ul>
            <div>
                <a href="http://newshop.dev/zh/new-products" title="本周新品汇总" class="btn btn-default button button-small"><span>本周新品汇总<i class="icon-chevron-right right"></i></span></a>
            </div>
            </div>
</div>
<!-- /MODULE Block new products --><?php }} ?>
