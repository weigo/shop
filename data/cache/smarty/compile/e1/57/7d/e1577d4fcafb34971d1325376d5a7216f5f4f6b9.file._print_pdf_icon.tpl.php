<?php /* Smarty version Smarty-3.1.19, created on 2016-03-17 14:06:14
         compiled from "/Volumes/sites/wwwroot/newshop.dev/admin/themes/default/template/controllers/slip/_print_pdf_icon.tpl" */ ?>
<?php /*%%SmartyHeaderCode:58809279656ea4956698b69-94776033%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    'e1577d4fcafb34971d1325376d5a7216f5f4f6b9' => 
    array (
      0 => '/Volumes/sites/wwwroot/newshop.dev/admin/themes/default/template/controllers/slip/_print_pdf_icon.tpl',
      1 => 1452070228,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '58809279656ea4956698b69-94776033',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'link' => 0,
    'order_slip' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.19',
  'unifunc' => 'content_56ea49566bb487_71289662',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_56ea49566bb487_71289662')) {function content_56ea49566bb487_71289662($_smarty_tpl) {?>



<a class="btn btn-default _blank" href="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['link']->value->getAdminLink('AdminPdf'), ENT_QUOTES, 'UTF-8', true);?>
&amp;submitAction=generateOrderSlipPDF&amp;id_order_slip=<?php echo intval($_smarty_tpl->tpl_vars['order_slip']->value->id);?>
">
	<i class="icon-file-text"></i>
	<?php echo smartyTranslate(array('s'=>'Download credit slip'),$_smarty_tpl);?>

</a>

<?php }} ?>
